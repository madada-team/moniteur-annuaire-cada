# moniteur-annuaire-cada

Ce dépôt est géré par l'équipe de [Ma Dada](https://madada.fr), et permet de suivre les changements apportés à [l'annuaire des PRADA distribué par la CADA](https://www.cada.fr/lacada/annuaire-des-prada).

Le code execute un script toutes les nuits pour détecter et sauvegarder les changements apportés à l'annuaire des PRADA
publié par la CADA.

Pour recevoir un mail à chaque changement sur le fichier, abonnez-vous à la MR [ici](!1).

# Comment ça marche

Le script télécharge le fichier à la source (site web de la CADA) toutes les nuits, le reformatte dans un format CSV stable, le trie dans un ordre prédéfini (et
arbitraire, mais toujours le même) et enregistre le CSV nettoyé. On applique ensuite un diff et on commit la nouvelle
version du fichier s'il a changé.

La merge request est mise à jour, et tous les "followers" sont notifiés du changement :)

Le diff de la Merge Request peut librement être consulté pour suivre les modifications apportées au fichier dans le temps. L'interface est un peu technique, mais n'hésitez pas à contacter l'équipe de Ma Dada si vous avez besoin d'aide.

# Maintenance

- Manage dependencies with poetry, then update requirements.txt with `poetry export -f requirements.txt --output
  requirements.txt`
- Only commit updates to the annuaire_prada.csv file in the MR branch (these changes should only be committed via the script, not manually)
- Changes to the code go into the `main` branch. Make sure to merge `main` into the MR branch for it to make use of the updated code!

## Mise à jour des certificats

Les certificats de la CADA n'ont pas l'air d'être automatiquement reconnus par `requests`. on télécharge donc la chaine
de certificats pour permettre de les valider. Le fichier est obtenu via un navigateur > cada.fr > certificats >
télécharger la chaine (PEM).
