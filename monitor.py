#!/bin/python3

# This script fetches the latest list of PRADAs (FOIA officers in France)
# normalises its format so that we can then apply a git diff cleanly.
# Using pandas since we need sorting.

import csv
from io import StringIO

import pandas as pd
import requests
from bs4 import BeautifulSoup

# fetch the main page to parse it for the link to the actual CSV file
page_url = "https://www.cada.fr/lacada/annuaire-des-prada"
page_response = requests.get(page_url, verify="cada_cert_chain.pem")
page_html = page_response.text

# extract the linnk to the csv file
soup = BeautifulSoup(page_html, "html.parser")
link = soup.find("article").a
assert link is not None, "Page broken: link CSS class not found"
url = link.get("href")
assert url.endswith(".csv")
if not url.startswith("https://cada.fr"):
    # fix relative url
    url = f"https://cada.fr{url}"
print(f"Downloading CSV file from {url}")

# the file name to output to
filename = "annuaire_prada.csv"

r = requests.get(url)  # , verify="cada_cert_chain.pem")

# it's not clear why the encoding is wrong without forcing it here
r.encoding = "utf-8"

# sometimes the file contains broken html <br> tags which can break
# the pandas csv parser, replace them
raw_csv = r.text.replace("<br>", "\n").replace("\r\n", "\t")
print(raw_csv[:1000])

df = pd.read_csv(
    StringIO(raw_csv),
    dtype=object,
    engine="python",
    header=0,
    # force autodetection of separator, combined with engine="python"
    sep=None,
    # this will cause pandas to skip malformed lines, for instance
    # a blank one at the end of the file
    on_bad_lines="warn",
)

# Expected columns (in this order):
# "Classement de l'administration",
# "Département de l'autorité",
# "Nom de l'administration",
# "Prénom PRADA","
# Nom PRADA",
# "Courriel 1 PRADA",
# "Adresse PRADA",
# "Code postal/Ville de l'autorité"

# sort values, first by dept, then type then name of admin, etc...
# This is arbitrary, and just meant to help cleanup the diff
df.sort_values(
    by=[
        "Département de l'autorité",
        "Classement de l'administration",
        "Nom de l'administration",
        "Prénom PRADA",
        "Nom PRADA",
    ],
    axis="index",
    inplace=True,
)

# standardise newlines to \n

df.to_csv(filename, index=False, quoting=csv.QUOTE_ALL)

print("CSV file updated. All done.")
